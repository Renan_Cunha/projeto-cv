export class CV {
  id: number;
  nome: string;
  email: string;
  cpf: number;
  telefone: number;
  data: string;
  sexo: string;
  formacao: string;
  qualificacoes: string[] = [];
  endereco: {
    cep: number;
    numero: number;
    complemento: string;
    rua: string;
    bairro: string;
    cidade: string;
    estado: string;
  }
}
