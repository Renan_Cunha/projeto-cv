import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  public transform(values: any[], filter: string): any[] {
    if (!values || !values.length) {
    return values;
    }
    if (!filter) {
    return values;
    }
    return values.filter(v => v.nome.toLocaleString().toLocaleLowerCase().includes(filter));
  }
}
