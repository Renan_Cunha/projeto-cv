import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, Output, Input, TemplateRef, ViewChild } from '@angular/core';

import { CV } from './home';
import { ConsultaCepService } from './services/consulta-cep.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @ViewChild('template', {static: false}) modal: TemplateRef<any>;
  formulario: FormGroup;
  cvUsuarios: CV[] = [];
  modalRef: BsModalRef;
  isEdit: boolean;
  idAtual = -1;
  qualiEdit: string = '';
  cvUsuarioEdit: any;
  busca: string;
  cvIndex: number;
  alreadyEmailExist: boolean = false;
  public maskPhone = ['(',  /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public maskCEP = [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];
  public maskCPF = [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];



  constructor(
    private formBuilder: FormBuilder,
    private cepService: ConsultaCepService,
    private modalService: BsModalService
    ) { }

  ngOnInit() {

    this.formulario = this.formBuilder.group({
      nome: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      cpf: [null, Validators.required],
      telefone: [null, Validators.required],
      data: [null],
      sexo: [null],
      formacao: [null],
      qualificacoes: [null],

      endereco: this.formBuilder.group({
        cep: [null],
        numero: [null],
        complemento: [null],
        rua: [null],
        bairro: [null],
        cidade: [null],
        estado: [null]
      })
    });
  /*  let cvUsuarios = localStorage.getItem("cvUsuarios");// Recupera os dados armazenados
    this.cvUsuarios = JSON.parse(cvUsuarios); // Converte string para objeto */
  }

  clearFieldEdit() {
    this.cvUsuarioEdit = new CV();
    this.formulario.reset();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, ({backdrop: 'static', keyboard: false}));
  }

  onSubmit() {
    if (!this.isEdit) {
      this.AddForm();
      return;
    }

    this.EditForm();
  }

  AddForm() {
    if (this.formulario.valid) {
      const usuario: CV = this.formulario.value;
      this.idAtual++;
      usuario.id = this.idAtual;
      usuario.qualificacoes = [];
      usuario.qualificacoes = this.cvUsuarioEdit.qualificacoes;
      this.cvUsuarios.push(usuario);
      // localStorage.setItem("cvUsuarios", JSON.stringify(this.cvUsuarios));
      console.log(this.cvUsuarios);
      this.modalRef.hide();
      this.formulario.reset(); // reseta o formulário no final
      return;
    }

    alert('Formulario inválido');
    this.verificaValidacoesForm(this.formulario);
  }

  EditForm() {
    if (this.formulario.valid) {
      const usuario: CV = this.formulario.value;
      usuario.qualificacoes = [];
      usuario.qualificacoes = this.cvUsuarioEdit.qualificacoes;
      this.cvUsuarios[this.cvIndex] = usuario;
      // localStorage.setItem("cvUsuarios", JSON.stringify(this.cvUsuarios));
      console.log(this.cvUsuarioEdit.qualificacoes);
      this.modalRef.hide();
      this.formulario.reset(); // reseta o formulário no final
      return;
    }

    alert('Formulario inválido');
    this.verificaValidacoesForm(this.formulario);
  }

  ClickEdit(user: any) {
    this.cvIndex = this.cvUsuarios.findIndex(v => v.email === user.email);

    if (this.cvIndex === -1) {
        return;
      }

    this.cvUsuarioEdit = this.cvUsuarios[this.cvIndex];
    this.populaDadosForm(this.cvUsuarioEdit);
    this.openModal(this.modal);
  }

  addQuali(valor: string) {
    this.qualiEdit = this.formulario.get(valor).value;

    if (this.cvUsuarioEdit.qualificacoes.length === 20) {
      alert ('Número máximo de qualificações alcançado');
      return;
    }

    if (this.qualiEdit === null || this.qualiEdit === '') {
      alert ('Não pode ser vazio!');
      return;
    }

    this.cvUsuarioEdit.qualificacoes.push(this.qualiEdit);
    this.qualiEdit = '';
  }

  deleteQuali(index: any) {
    this.cvUsuarioEdit.qualificacoes.splice(index, 1);
  }

  DeleteForm(user: any) {
    this.cvIndex = this.cvUsuarios.findIndex(v => v.email === user.email);

    if (this.cvIndex === -1) {
        return;
      }

    this.cvUsuarios.splice(this.cvIndex, 1);

    // localStorage.setItem("cvUsuarios", JSON.stringify(this.cvUsuarios));
  }

  verificaValidacoesForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(campo => {
      const controle = formGroup.get(campo);
      controle.markAsDirty();
    });
  }

  aplicaCssErro(campo: string) {
    return !this.formulario.get(campo).valid &&
    (this.formulario.get(campo).touched || this.formulario.get(campo).dirty);
  }

  consultaCEP() {
    const cep = this.formulario.get('endereco.cep').value;

    if (cep != null && cep !== '') {
      this.cepService.consultaCEP(cep)
      .subscribe(dados => this.populaDadosCEP(dados));
    }
  }

  populaDadosForm(dados: any) {
    this.formulario.patchValue({
      nome: dados.nome,
      email: dados.email,
      cpf: dados.cpf,
      telefone: dados.telefone,
      data: dados.data,
      sexo: dados.sexo,
      formacao: dados.formacao,
      endereco: {
        cep: dados.endereco.cep,
        rua: dados.endereco.rua,
        numero: dados.endereco.numero,
        complemento: dados.endereco.complemento,
        bairro: dados.endereco.bairro,
        cidade: dados.endereco.cidade,
        estado: dados.endereco.estado
      }
    });
  }

  populaDadosCEP(dados: any) {
    this.formulario.patchValue({
      endereco: {
        rua: dados.logradouro,
        cep: dados.cep,
        complemento: dados.complemento,
        bairro: dados.bairro,
        cidade: dados.localidade,
        estado: dados.uf
      }
    });
  }


  validarEmail() {
    let input = this.formulario.get('email').value;

    if (!input) {
      return;
    }

    if (!this.cvUsuarios  || this.cvUsuarios.length === 0) {
      return false;
    }

    this.alreadyEmailExist = false;

    for (let i = 0; i < this.cvUsuarios.length; i++) {
      if (this.isEdit && this.cvUsuarios[i].id === this.cvUsuarioEdit.id) {
        continue  ;
      }

      if (input === this.cvUsuarios[i].email) {
        this.alreadyEmailExist = true;
        return true;
      }
    }
  }

}
