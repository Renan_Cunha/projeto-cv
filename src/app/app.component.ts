import { CV } from './home/home';
import { Component, Input } from '@angular/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  Usuarios: CV[];
  showForm: boolean;



constructor(){

  var Usuarios = localStorage.getItem("cvUsuarios");// Recupera os dados armazenados
    this.Usuarios = JSON.parse(Usuarios); // Converte string para objeto
    if(Usuarios == null)
      this.Usuarios = [];
}

openForm():void {
  this.showForm = !this.showForm;
}

  title = 'projeto-cv';
}
